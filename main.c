#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <mysql/mysql.h>

#define STRING_LIMIT 100

int port;
char host[STRING_LIMIT]   = "";
char user[STRING_LIMIT]   = "";
char pwd[STRING_LIMIT]    = "";
char dbname[STRING_LIMIT] = "";


void usage (void);

int main(int argc, char *argv[])
{
    int opchar;
    char tableDES[300];

    // 获取参数
    while ((opchar = getopt(argc, argv, "hH:P:U:W:N:")) != EOF) {
        switch (opchar) {
            case 'H':
                strcpy (host, optarg);
                break;
            case 'P':
                port = 3306;
                break;
            case 'U':
                strcpy (user, optarg);
                break;
            case 'W':
                strcpy (pwd, optarg);
                break;
			case 'N':
				strcpy (dbname, optarg);
				break;
            default:
                usage();
                return 0;
        }
    }

    // 检验用户输入的参数
    if (host[0] == 0) {
        printf ("输入数据库地址: ");
        scanf ("%s", host);
    }
    if (port == 0) {
        printf ("输入数据库端口号: ");
        scanf ("%d", &port);
    }
    if (user[0] == 0) {
        printf ("输入用户名: ");
        scanf ("%s", user);
    }
    if (pwd[0] == 0) {
        printf ("输入数据库密码: ");
        scanf ("%s", pwd);
    }
	if (dbname[0] == 0) {
		printf ("输入库名：");
		scanf ("%s", dbname);
	}
    
	MYSQL mysql;
	MYSQL_RES *table_list;
	MYSQL_ROW table_row;

	mysql_init (&mysql);
	mysql_options (&mysql, MYSQL_SET_CHARSET_NAME, "utf8");
	mysql_real_connect (&mysql, host, user, pwd, dbname, port, NULL, 0);

	FILE *fp;
	fp = fopen("doc.md", "w+");


	char get_table_sql[300] = "select table_name,TABLE_COMMENT from INFORMATION_SCHEMA.TABLES where table_schema = \'";
	strcat (get_table_sql, dbname);
	strcat (get_table_sql, "\'");
	
	mysql_query(&mysql, get_table_sql);
	table_list = mysql_store_result(&mysql);

	while (table_row = mysql_fetch_row (table_list)) {
		fprintf (fp, "\n## %s - %s\n\n", table_row[0], table_row[1]);
		char get_column_sql[300] = "";
		strcat (get_column_sql, "show full columns from ");
		strcat (get_column_sql, table_row[0]);

		MYSQL_RES *table_column;
		MYSQL_ROW column_row;
	
		printf ("%s\n", get_column_sql);
		mysql_query (&mysql, get_column_sql);
		table_column = mysql_store_result (&mysql);

		fprintf (fp, "字段名 | 类型 | 编码 | 是否为空 | key | 默认值 | Extra | 描述\n");
		fprintf (fp, "------ | ---- | ---- | -------- | --- | ------ | ----- | ----\n");
		while (column_row = mysql_fetch_row (table_column)) {
			fprintf (fp, "%s | %s | %s | %s | %s | %s | %s | %s\n", column_row[0], column_row[1]
					, column_row[2], column_row[3], column_row[4]
					, column_row[5], column_row[6], column_row[8]);
		}
		mysql_free_result (table_column);
	}
	fclose (fp);

	mysql_free_result (table_list);
    return 0;
}

/**
 * 使用帮助
 */
void usage (void)
{
    printf ("Usage: mysql_doc -[h] [-H host] [-P port] [-U username] [-W passwd] [-N dbname]\n");
    printf ("-h: help!\n");
    printf ("-H host: mysql host \n");
    printf ("-P port: mysql port same as 3306\n");
    printf ("-U username: mysql user name\n");
    printf ("-W passwd: mysql password\n");
	printf ("-N dbname: database name\n");
}

